package assignment.app.constant;

/**
 * Created by Mukesh on 17/04/18.
 */

public class Constants {

    public interface IMimeUtils {
        String IMAGES = "image";
        String DOCS = "text";
        String VIDEOS = "video";
        String MUSIC = "audio";
    }

    public interface IBundleUtils {
        String LIST_DATA = "listData";
        String IMAGES = "Images";
        String DOCS = "Docs";
        String VIDEOS = "Videos";
        String MUSIC = "Music";
        String OTHER = "Other";
        String FREE = "Free";
        String TOTAL_SPACE = "totalMemory";
        String USED_SPACE = "userSpace";
        String FREE_SPACE = "FreeSpace";
        String AVERAGE_FILE_SIZE = "averagaFileSize";
        String IS_BOND = "IsBond";
        String IS_DATA_FETCHED = "IsDataFetched";
    }
}
