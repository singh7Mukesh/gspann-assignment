package assignment.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.util.List;

import assignment.app.R;
import assignment.app.databinding.AdapterFileBinding;
import assignment.app.entity.MyFile;
import assignment.app.utils.StorageUtils;

/**
 * Created by Mukesh on 18-04-2018.
 */
public class RVAdapterFiles extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<File> mFiles;
    private StorageUtils mStorage;

    public RVAdapterFiles(Context context) {
        mStorage = new StorageUtils(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_file, parent, false);
        return new FileViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final File file = mFiles.get(position);
        FileViewHolder fileViewHolder = (FileViewHolder) holder;
        MyFile myFile = new MyFile();
        myFile.setName(file.getName());
        myFile.setRedableSize(mStorage.getReadableSize(file));
        fileViewHolder.binding.setMyFile(myFile);
    }

    @Override
    public int getItemCount() {
        return mFiles != null ? mFiles.size() : 0;
    }

    public void setFiles(List<File> files) {
        mFiles = files;
    }

    static class FileViewHolder extends RecyclerView.ViewHolder {
        AdapterFileBinding binding;

        FileViewHolder(View v) {
            super(v);
            binding = DataBindingUtil.bind(v);
        }
    }
}
