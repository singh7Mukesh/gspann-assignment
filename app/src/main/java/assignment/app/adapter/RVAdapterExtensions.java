package assignment.app.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import assignment.app.R;
import assignment.app.databinding.AdapterExtensionsBinding;
import assignment.app.entity.FileExtension;
import assignment.app.utils.SizeUtility;

/**
 * Created by Mukesh on 18-04-2018.
 */
public class RVAdapterExtensions extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<FileExtension> mFiles;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_extensions, parent, false);
        return new ExtensionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final FileExtension fileExtension = mFiles.get(position);
        ExtensionViewHolder fileViewHolder = (ExtensionViewHolder) holder;
        fileExtension.setRedableSize(SizeUtility.readableSizeUnit(fileExtension.getSize()));
        fileViewHolder.binding.setFileExtension(fileExtension);
    }

    @Override
    public int getItemCount() {
        return mFiles != null ? mFiles.size() : 0;
    }

    public void setFiles(List<FileExtension> files) {
        mFiles = files;
    }


    static class ExtensionViewHolder extends RecyclerView.ViewHolder {

        AdapterExtensionsBinding binding;

        ExtensionViewHolder(View v) {
            super(v);
            binding = DataBindingUtil.bind(v);
        }
    }

}
