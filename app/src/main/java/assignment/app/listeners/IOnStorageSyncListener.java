package assignment.app.listeners;

import android.os.Bundle;

/**
 * Created by Mukesh on 19-04-2018.
 */

public interface IOnStorageSyncListener {

    void onSubscribed();

    void onProgress(int progress);

    void onSuccessSync(Bundle bundle);

}
