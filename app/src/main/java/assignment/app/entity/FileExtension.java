package assignment.app.entity;

/**
 * Created by Mukesh on 18-04-2018.
 */

public class FileExtension {

    private String extension;
    private int color;
    private long size;
    private String redableSize;

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getRedableSize() {
        return redableSize;
    }

    public void setRedableSize(String redableSize) {
        this.redableSize = redableSize;
    }
}
