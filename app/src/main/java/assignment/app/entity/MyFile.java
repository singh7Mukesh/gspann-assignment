package assignment.app.entity;

/**
 * Created by Mukesh on 19-04-2018.
 */

public class MyFile {

    private String name;
    private String redableSize;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRedableSize() {
        return redableSize;
    }

    public void setRedableSize(String redableSize) {
        this.redableSize = redableSize;
    }
}
