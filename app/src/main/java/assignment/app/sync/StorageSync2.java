package assignment.app.sync;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;

import assignment.app.constant.Constants;
import assignment.app.listeners.IOnStorageSyncListener;
import assignment.app.utils.CompareType;
import assignment.app.utils.StorageUtils;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Mukesh on 27-04-2018.
 */

public class StorageSync2 {

    private static StorageSync2 instance;
    private final int FIXED_SIZE = 10;
    private List<String> listFilePaths = new ArrayList<>();
    private List<File> listFiles = new ArrayList<>();
    private Timer mTimer;
    private int incrementBy = 2;
    private StorageUtils mStorage;
    private long totalSpace, usedSpace, freeSpace, averageFileSize,
            musicsFileSize, docsFileSize, imagesFileSize, videosFileSize, othersFileSize,
            totalNumberOfFiles;
    private boolean isAnalysisCompleted;
    private int progress;
    private IOnStorageSyncListener listener;

    private StorageSync2(Context context) {
        mStorage = new StorageUtils(context);
    }

    public static StorageSync2 getInstance(Context context) {
        if (instance == null) {
            synchronized (StorageSync2.class) {
                if (instance == null)
                    instance = new StorageSync2(context);
            }
        }
        return instance;
    }

    public void registerCallBack(IOnStorageSyncListener listener) {
        this.listener = listener;
    }


    public void startSync() {
        mTimer = new Timer();
        observable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Bundle>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        DisposableManager.add(d);
                        if (listener != null)
                            listener.onSubscribed();
                        mTimer.scheduleAtFixedRate(new MyTask(), 1000, 100L);
                    }

                    @Override
                    public void onNext(Bundle bundle) {
                        if (listener != null)
                            listener.onSuccessSync(bundle);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    Observable<Bundle> observable = Observable.fromCallable(new Callable<Bundle>() {

        @Override
        public Bundle call() throws Exception {
            return fetchFiles();
        }
    });

    private Bundle fetchFiles() {
        try {
            loadFiles(Environment.getExternalStorageDirectory().getAbsolutePath());
            freeSpace = mStorage.getFreeSpace(mStorage.getExternalStorageDirectory());
            usedSpace = mStorage.getUsedSpace(mStorage.getExternalStorageDirectory());
            totalSpace = freeSpace + usedSpace;
            averageFileSize = usedSpace / totalNumberOfFiles;
            Collections.reverse(listFiles);
            for (File file : listFiles)
                listFilePaths.add(file.getAbsolutePath());
            return returnResult();
        } catch (Exception e) {
            e.printStackTrace();
            incrementBy = 50;
            isAnalysisCompleted = true;
        }
        return null;
    }

    public void loadFiles(String path) {
        File file = new File(path);
        getDirectoryFilesImpl(file);
    }

    private void getDirectoryFilesImpl(File directory) {
        if (directory.exists()) {
            File[] files = directory.listFiles();
            if (files == null) {
                return;
            } else {
                for (int i = 0; i < files.length; i++) {
                    if (files[i].isDirectory()) {
                        getDirectoryFilesImpl(files[i]);
                    } else {
                        updateResult(files[i]);
                        totalNumberOfFiles++;
                    }
                }
            }
        }
    }

    private void updateResult(File currentFile) {
        Uri selectedUri = Uri.fromFile(currentFile);
        long currentFileSize = mStorage.getSize(currentFile);
        String fileExtension1
                = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
        String mimeType
                = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension1);
        if (mimeType == null || mimeType.trim().equals(""))
            othersFileSize += currentFileSize;
        else {
            if (mimeType.contains(Constants.IMimeUtils.IMAGES))
                imagesFileSize += currentFileSize;
            else if (mimeType.contains(Constants.IMimeUtils.DOCS))
                docsFileSize += currentFileSize;
            else if (mimeType.contains(Constants.IMimeUtils.VIDEOS))
                videosFileSize += currentFileSize;
            else if (mimeType.contains(Constants.IMimeUtils.MUSIC))
                musicsFileSize += currentFileSize;
            else
                othersFileSize += currentFileSize;
        }
        if (listFiles.size() == FIXED_SIZE) {
            File mFile = listFiles.get(0);
            long mFileSize = mStorage.getSize(mFile);
            if (currentFileSize > mFileSize) {
                listFiles.remove(0);
                listFiles.add(0, currentFile);
                Collections.sort(listFiles, CompareType.SIZE.getComparator());
            }
        } else {
            listFiles.add(currentFile);
            Collections.sort(listFiles, CompareType.SIZE.getComparator());
        }
    }

    private Bundle returnResult() {
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(Constants.IBundleUtils.LIST_DATA, new ArrayList<>(listFilePaths));
        bundle.putLong(Constants.IBundleUtils.USED_SPACE, usedSpace);
        bundle.putLong(Constants.IBundleUtils.FREE_SPACE, freeSpace);
        bundle.putLong(Constants.IBundleUtils.TOTAL_SPACE, totalSpace);
        bundle.putLong(Constants.IBundleUtils.AVERAGE_FILE_SIZE, averageFileSize);
        bundle.putLong(Constants.IBundleUtils.DOCS, docsFileSize);
        bundle.putLong(Constants.IBundleUtils.MUSIC, musicsFileSize);
        bundle.putLong(Constants.IBundleUtils.VIDEOS, videosFileSize);
        bundle.putLong(Constants.IBundleUtils.OTHER, othersFileSize);
        bundle.putLong(Constants.IBundleUtils.IMAGES, imagesFileSize);
        incrementBy = 50;
        isAnalysisCompleted = true;
        return bundle;
    }

    public void stopSync() {
        if (mTimer != null)
            mTimer.cancel();
        DisposableManager.dispose();
        progress = 0;
        incrementBy = 2;
        listener = null;
        isAnalysisCompleted = false;
        if (listFilePaths != null)
            listFilePaths.clear();
        if (listFiles != null)
            listFiles.clear();
        totalSpace = 0;
        usedSpace = 0;
        freeSpace = 0;
        averageFileSize = 0;
        musicsFileSize = 0;
        docsFileSize = 0;
        imagesFileSize = 0;
        videosFileSize = 0;
        othersFileSize = 0;
        totalNumberOfFiles = 0;
    }

    private class MyTask extends TimerTask {
        @Override
        public void run() {
            try {
                if (isAnalysisCompleted) {
                    if (listener != null)
                        listener.onProgress(100);
                    stopSync();
                } else {
                    if (progress >= 30 && progress < 80) {
                        incrementBy = 1;
                    } else if (progress >= 90) {
                        incrementBy = 0;
                    }
                    progress += incrementBy;
                    if (listener != null)
                        listener.onProgress(progress);
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
    }
}
