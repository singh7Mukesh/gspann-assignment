package assignment.app.sync;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;

import assignment.app.constant.Constants;
import assignment.app.listeners.IOnStorageSyncListener;
import assignment.app.utils.CompareType;
import assignment.app.utils.StorageUtils;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Mukesh on 19-04-2018.
 */

public class StorageSync {

    private static StorageSync instance;
    private ArrayList<String> listFiles = new ArrayList<>();
    private StorageUtils mStorage;
    private HashMap<String, String> finalStats = new HashMap<>();
    private List<File> listAllFiles;
    private Timer mTimer;
    private int incrementBy = 3;
    private long totalSpace, usedSpace, freeSpace, averageFileSize;
    private boolean isAnalysisCompleted;
    private int progress;
    private IOnStorageSyncListener listener;

    private StorageSync(Context context) {
        mStorage = new StorageUtils(context);
    }

    public static StorageSync getInstance(Context context) {
        if (instance == null) {
            synchronized (StorageSync.class) {
                if (instance == null)
                    instance = new StorageSync(context);
            }
        }
        return instance;
    }

    public void registerCallBack(IOnStorageSyncListener listener) {
        this.listener = listener;
    }


    public void stopSync() {
        if (mTimer != null)
            mTimer.cancel();
        DisposableManager.dispose();
        progress = 0;
        incrementBy = 5;
        listener = null;
        isAnalysisCompleted = false;
    }

    public void startSync() {
        mTimer = new Timer();
        observable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Bundle>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        DisposableManager.add(d);
                        if (listener != null)
                            listener.onSubscribed();
                        mTimer.scheduleAtFixedRate(new MyTask(), 1000, 100L);
                    }

                    @Override
                    public void onNext(Bundle bundle) {
                        if (listener != null)
                            listener.onSuccessSync(bundle);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    Observable<Bundle> observable = Observable.fromCallable(new Callable<Bundle>() {

        @Override
        public Bundle call() throws Exception {
            return fetchFiles();
        }
    });

    private Bundle fetchFiles() {
        try {
            List<File> files = mStorage.getNestedFiles(mStorage.getExternalStorageDirectory());
            if (files != null) {
                Collections.sort(files, CompareType.SIZE.getComparator());
                Collections.reverse(files);
            }
            freeSpace = mStorage.getFreeSpace(mStorage.getExternalStorageDirectory());
            usedSpace = mStorage.getUsedSpace(mStorage.getExternalStorageDirectory());
            totalSpace = freeSpace + usedSpace;
            averageFileSize = usedSpace / files.size();
            List<File> top10Files = new ArrayList<>();
            top10Files.addAll(files);
            if (top10Files != null && top10Files.size() > 10)
                top10Files.subList(10, top10Files.size()).clear();
            for (File file : top10Files)
                listFiles.add(file.getAbsolutePath());
            listAllFiles = files;
            return setupAnalysis();
        } catch (Exception e) {
            e.printStackTrace();
            incrementBy = 50;
            isAnalysisCompleted = true;
        }
        return null;
    }


    private Bundle setupAnalysis() {
        HashMap<String, List<File>> fileStats = new HashMap<>();
        fileStats.put(Constants.IBundleUtils.IMAGES, new ArrayList<File>());
        fileStats.put(Constants.IBundleUtils.DOCS, new ArrayList<File>());
        fileStats.put(Constants.IBundleUtils.VIDEOS, new ArrayList<File>());
        fileStats.put(Constants.IBundleUtils.MUSIC, new ArrayList<File>());
        fileStats.put(Constants.IBundleUtils.OTHER, new ArrayList<File>());
        for (File file : listAllFiles) {
            Uri selectedUri = Uri.fromFile(file);
            String fileExtension1
                    = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
            String mimeType
                    = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension1);
            if (mimeType == null || mimeType.trim().equals(""))
                fileStats.get(Constants.IBundleUtils.OTHER).add(file);
            else {
                if (mimeType.contains(Constants.IMimeUtils.IMAGES))
                    fileStats.get(Constants.IBundleUtils.IMAGES).add(file);
                else if (mimeType.contains(Constants.IMimeUtils.DOCS))
                    fileStats.get(Constants.IBundleUtils.DOCS).add(file);
                else if (mimeType.contains(Constants.IMimeUtils.VIDEOS))
                    fileStats.get(Constants.IBundleUtils.VIDEOS).add(file);
                else if (mimeType.contains(Constants.IMimeUtils.MUSIC))
                    fileStats.get(Constants.IBundleUtils.MUSIC).add(file);
                else
                    fileStats.get(Constants.IBundleUtils.OTHER).add(file);
            }
        }

        if (fileStats != null && fileStats.size() > 0) {
            Iterator it = fileStats.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                List<File> mimeFiles = (List<File>) pair.getValue();
                long size = 0;
                if (mimeFiles != null && mimeFiles.size() > 0) {
                    for (File file : mimeFiles) {
                        size += mStorage.getSize(file);
                    }
                    finalStats.put((String) pair.getKey(), String.valueOf(size));
                } else {
                    finalStats.put((String) pair.getKey(), String.valueOf(size));
                }
                it.remove();
            }
        }
        incrementBy = 50;
        isAnalysisCompleted = true;
        return returnResult();
    }

    private Bundle returnResult() {
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(Constants.IBundleUtils.LIST_DATA, listFiles);
        bundle.putLong(Constants.IBundleUtils.USED_SPACE, usedSpace);
        bundle.putLong(Constants.IBundleUtils.FREE_SPACE, freeSpace);
        bundle.putLong(Constants.IBundleUtils.TOTAL_SPACE, totalSpace);
        bundle.putLong(Constants.IBundleUtils.AVERAGE_FILE_SIZE, averageFileSize);
        try {
            Iterator it = finalStats.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                bundle.putLong((String) pair.getKey(), Long.parseLong((String) pair.getValue()));
                it.remove();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bundle;
    }

    private class MyTask extends TimerTask {
        @Override
        public void run() {
            try {
                if (isAnalysisCompleted) {
                    if (listener != null)
                        listener.onProgress(100);
                    stopSync();
                } else {
                    if (progress >= 30 && progress < 80) {
                        incrementBy = 1;
                    } else if (progress >= 90) {
                        incrementBy = 0;
                    }
                    progress += incrementBy;
                    if (listener != null)
                        listener.onProgress(progress);
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
    }
}
