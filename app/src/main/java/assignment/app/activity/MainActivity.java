package assignment.app.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import assignment.app.R;
import assignment.app.adapter.RVAdapterExtensions;
import assignment.app.constant.Constants;
import assignment.app.custom.CustomPieChartHelper;
import assignment.app.custom.CustomPieChartView;
import assignment.app.databinding.ActivityMainBinding;
import assignment.app.entity.FileExtension;
import assignment.app.listeners.IOnStorageSyncListener;
import assignment.app.sync.StorageSync2;
import assignment.app.utils.SizeUtility;

public class MainActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_CODE = 1000;
    ActivityMainBinding binding;
    private RVAdapterExtensions mFilesExtensionAdapter;
    private boolean mIsBound, mIsDataFetched, isForcedStop;
    private long musicsFileSize, docsFileSize, imagesFileSize, videosFileSize, othersFileSize,
            totalSpace, usedSpace, freeSpace, averageFileSize;
    private List<FileExtension> listExtensions;
    private final int[] COLOR_LIST = {Color.parseColor("#33B5E5"),
            Color.parseColor("#AA66CC"),
            Color.parseColor("#99CC00"),
            Color.parseColor("#FFBB33"),
            Color.parseColor("#FF4444"),
            Color.parseColor("#c6c2c2")};
    private ArrayList<String> listFiles = new ArrayList<>();
    private NotificationManager mNotificationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        GridLayoutManager lLayout = new GridLayoutManager(this, 2);
        binding.recycler.setHasFixedSize(true);
        binding.recycler.setLayoutManager(lLayout);
        mFilesExtensionAdapter = new RVAdapterExtensions();
        binding.recycler.setAdapter(mFilesExtensionAdapter);

        binding.setHandlers(this);
    }

    public void onClickShowList(View view) {
        Intent intent = new Intent(MainActivity.this, ListActivity.class);
        intent.putExtra(Constants.IBundleUtils.LIST_DATA, listFiles);
        startActivity(intent);
    }

    public void onClickCheckForPermissions(View view) {
        isWritePermissionGranted();
    }

    public void onClickForceStop(View view) {
        isForcedStop = true;
        doUnbindService();
    }

    public void onClickShareStats(View view) {
        binding.btnTxtShare.setVisibility(View.INVISIBLE);
        binding.linearFinal.setDrawingCacheEnabled(false);
        binding.linearFinal.setDrawingCacheEnabled(true);
        final Bitmap bitmap = binding.linearFinal.getDrawingCache();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                share(bitmap);
            }
        }, 500);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (!mIsBound && mIsDataFetched) {
            outState.putStringArrayList(Constants.IBundleUtils.LIST_DATA, listFiles);
            outState.putLong(Constants.IBundleUtils.USED_SPACE, usedSpace);
            outState.putLong(Constants.IBundleUtils.FREE_SPACE, freeSpace);
            outState.putLong(Constants.IBundleUtils.TOTAL_SPACE, totalSpace);
            outState.putLong(Constants.IBundleUtils.AVERAGE_FILE_SIZE, averageFileSize);
            outState.putLong(Constants.IBundleUtils.DOCS, docsFileSize);
            outState.putLong(Constants.IBundleUtils.MUSIC, musicsFileSize);
            outState.putLong(Constants.IBundleUtils.VIDEOS, videosFileSize);
            outState.putLong(Constants.IBundleUtils.OTHER, othersFileSize);
            outState.putLong(Constants.IBundleUtils.IMAGES, imagesFileSize);
        }
        outState.putBoolean(Constants.IBundleUtils.IS_BOND, mIsBound);
        outState.putBoolean(Constants.IBundleUtils.IS_DATA_FETCHED, mIsDataFetched);
    }

    @Override
    protected void onRestoreInstanceState(Bundle restore) {
        super.onRestoreInstanceState(restore);
        if (restore == null)
            return;
        mIsBound = restore.getBoolean(Constants.IBundleUtils.IS_BOND);
        mIsDataFetched = restore.getBoolean(Constants.IBundleUtils.IS_DATA_FETCHED);
        if (!mIsBound) {
            if (mIsDataFetched) {
                listFiles = restore.getStringArrayList(Constants.IBundleUtils.LIST_DATA);
                usedSpace = restore.getLong(Constants.IBundleUtils.USED_SPACE);
                freeSpace = restore.getLong(Constants.IBundleUtils.FREE_SPACE);
                totalSpace = restore.getLong(Constants.IBundleUtils.TOTAL_SPACE);
                averageFileSize = restore.getLong(Constants.IBundleUtils.AVERAGE_FILE_SIZE);
                docsFileSize = restore.getLong(Constants.IBundleUtils.DOCS);
                musicsFileSize = restore.getLong(Constants.IBundleUtils.MUSIC);
                videosFileSize = restore.getLong(Constants.IBundleUtils.VIDEOS);
                othersFileSize = restore.getLong(Constants.IBundleUtils.OTHER);
                imagesFileSize = restore.getLong(Constants.IBundleUtils.IMAGES);
                updatePieChart(binding.pieView);
            }
        } else {
            doBindService();
        }
    }

    public boolean isWritePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                doBindService();
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                return false;
            }
        } else {
            doBindService();
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[]
            grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                doBindService();
            } else {
                isWritePermissionGranted();
            }
        }
    }

    private void doBindService() {
        mIsBound = true;
        isForcedStop = false;
        binding.btnTxtStart.setVisibility(View.GONE);
        binding.btnTxtStop.setVisibility(View.VISIBLE);
        binding.txtViewScan.setText("Scanning your External Storage");
        StorageSync2.getInstance(getApplicationContext()).registerCallBack(syncListener);
        StorageSync2.getInstance(getApplicationContext()).startSync();
    }

    private IOnStorageSyncListener syncListener = new IOnStorageSyncListener() {
        @Override
        public void onSubscribed() {
            showNotification();
        }

        @Override
        public void onProgress(int progress) {
            binding.progressBar.setProgress(progress);
            binding.progressBar.setSecondaryProgress(progress + 5);
        }

        @Override
        public void onSuccessSync(Bundle bundle) {
            mIsDataFetched = true;
            doUnbindService();
            listFiles = bundle.getStringArrayList(Constants.IBundleUtils.LIST_DATA);
            musicsFileSize = bundle.getLong(Constants.IBundleUtils.MUSIC);
            docsFileSize = bundle.getLong(Constants.IBundleUtils.DOCS);
            imagesFileSize = bundle.getLong(Constants.IBundleUtils.IMAGES);
            videosFileSize = bundle.getLong(Constants.IBundleUtils.VIDEOS);
            othersFileSize = bundle.getLong(Constants.IBundleUtils.OTHER);
            totalSpace = bundle.getLong(Constants.IBundleUtils.TOTAL_SPACE);
            usedSpace = bundle.getLong(Constants.IBundleUtils.USED_SPACE);
            freeSpace = bundle.getLong(Constants.IBundleUtils.FREE_SPACE);
            averageFileSize = bundle.getLong(Constants.IBundleUtils.AVERAGE_FILE_SIZE);
            updatePieChart(binding.pieView);
        }
    };

    private void doUnbindService() {
        mIsBound = false;
        binding.btnTxtStart.setVisibility(View.VISIBLE);
        binding.btnTxtStop.setVisibility(View.GONE);
        binding.txtViewScan.setText("Scan your External Storage");
        binding.progressBar.setProgress(0);
        binding.progressBar.setSecondaryProgress(0);
        if (isForcedStop)
            StorageSync2.getInstance(getApplicationContext()).stopSync();
        if (mNotificationManager != null)
            mNotificationManager.cancelAll();
    }

    private void showNotification() {
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
        Notification notification = new Notification.Builder(this)
                .setContentTitle("Scanning Files")
                .setContentText("Wait...").setSmallIcon(R.drawable.ic_notification)
                .setContentIntent(contentIntent).build();
        mNotificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notification.flags = notification.flags
                | Notification.FLAG_ONGOING_EVENT;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        mNotificationManager.notify(0, notification);
    }

    private void switchToFinalResult() {
        binding.btnTxtShare.setEnabled(true);
        binding.linearMain.setVisibility(View.GONE);
        binding.linearFinal.setVisibility(View.VISIBLE);
    }

    private void updatePieChart(CustomPieChartView pieView) {
        switchToFinalResult();
        listExtensions = new ArrayList<>();
        FileExtension extension = new FileExtension();
        DecimalFormat decimalFormat = new DecimalFormat("#.##");

        double music = musicsFileSize / (double) totalSpace;
        float musicPercent = Float.parseFloat(decimalFormat.format(music));
        extension.setColor(COLOR_LIST[0]);
        extension.setExtension(Constants.IBundleUtils.MUSIC);
        extension.setSize(musicsFileSize);
        listExtensions.add(extension);

        double video = videosFileSize / (double) totalSpace;
        float videoPercent = Float.parseFloat(decimalFormat.format(video));
        extension = new FileExtension();
        extension.setColor(COLOR_LIST[1]);
        extension.setExtension(Constants.IBundleUtils.VIDEOS);
        extension.setSize(videosFileSize);
        listExtensions.add(extension);

        double image = imagesFileSize / (double) totalSpace;
        float imagePercent = Float.parseFloat(decimalFormat.format(image));
        extension = new FileExtension();
        extension.setColor(COLOR_LIST[2]);
        extension.setExtension(Constants.IBundleUtils.IMAGES);
        extension.setSize(imagesFileSize);
        listExtensions.add(extension);

        double doc = docsFileSize / (double) totalSpace;
        float docPercent = Float.parseFloat(decimalFormat.format(doc));
        extension = new FileExtension();
        extension.setColor(COLOR_LIST[3]);
        extension.setExtension(Constants.IBundleUtils.DOCS);
        extension.setSize(docsFileSize);
        listExtensions.add(extension);

        double other = othersFileSize / (double) totalSpace;
        float otherPercent = Float.parseFloat(decimalFormat.format(other));
        extension = new FileExtension();
        extension.setColor(COLOR_LIST[4]);
        extension.setExtension(Constants.IBundleUtils.OTHER);
        extension.setSize(othersFileSize);
        listExtensions.add(extension);

        float freePercent = 1 - (musicPercent + videoPercent + imagePercent + docPercent + otherPercent);
        extension = new FileExtension();
        extension.setColor(COLOR_LIST[5]);
        extension.setExtension(Constants.IBundleUtils.FREE);
        extension.setSize(freeSpace);
        listExtensions.add(extension);

        ArrayList<CustomPieChartHelper> pieHelperArrayList = new ArrayList<>();
        pieHelperArrayList.add(new CustomPieChartHelper(musicPercent * 100, Constants.IBundleUtils.MUSIC, COLOR_LIST[0]));
        pieHelperArrayList.add(new CustomPieChartHelper(videoPercent * 100, Constants.IBundleUtils.VIDEOS, COLOR_LIST[1]));
        pieHelperArrayList.add(new CustomPieChartHelper(imagePercent * 100, Constants.IBundleUtils.IMAGES, COLOR_LIST[2]));
        pieHelperArrayList.add(new CustomPieChartHelper(docPercent * 100, Constants.IBundleUtils.DOCS, COLOR_LIST[3]));
        pieHelperArrayList.add(new CustomPieChartHelper(otherPercent * 100, Constants.IBundleUtils.OTHER, COLOR_LIST[4]));
        pieHelperArrayList.add(new CustomPieChartHelper(freePercent * 100, Constants.IBundleUtils.FREE, COLOR_LIST[5]));

        pieView.setDate(pieHelperArrayList);

        mFilesExtensionAdapter.setFiles(listExtensions);
        mFilesExtensionAdapter.notifyDataSetChanged();

        binding.totalSize.setText("Total Space : " + SizeUtility.readableSizeUnit(totalSpace));
        binding.totalAverageSize.setText("Average File Size : " + SizeUtility.readableSizeUnit(averageFileSize));
    }


    private void share(Bitmap icon) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        icon.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
        try {
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
        share.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temporary_file.jpg"));
        startActivity(Intent.createChooser(share, "Sharing statistics"));
        binding.btnTxtShare.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        doUnbindService();
    }
}
