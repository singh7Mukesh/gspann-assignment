package assignment.app.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import assignment.app.R;
import assignment.app.adapter.RVAdapterFiles;
import assignment.app.constant.Constants;
import assignment.app.databinding.ActivityListBinding;

/**
 * Created by Mukesh on 18-04-2018.
 */

public class ListActivity extends AppCompatActivity {

    ActivityListBinding binding;
    private RVAdapterFiles mFilesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_list);

        ArrayList<String> listFiles = this.getIntent().getStringArrayListExtra(Constants.IBundleUtils.LIST_DATA);
        List<File> top10Files = new ArrayList<>();
        if (listFiles != null)
            for (String path : listFiles) {
                top10Files.add(new File(path));
            }
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        binding.recycler.setLayoutManager(layoutManager);
        mFilesAdapter = new RVAdapterFiles(getApplicationContext());
        mFilesAdapter.setFiles(top10Files);
        binding.recycler.setAdapter(mFilesAdapter);
    }
}
